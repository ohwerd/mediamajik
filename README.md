# README #

This README documents the normal steps to get the MediaMajik app running.

1. Navigate to the app directory in terminal.
2. Enter command ```rails s``` to start the webserver on your localhost (the default port is 3000).
3. Navigate to localhost:3000 on your browser to see the app running on the default port.

### What is this repository for? ###

* Quick summary: This app is made as part of project 2 of the Software Modelling and Design class taught at the University of Melbourne
* Version: 0.0.0

### TODO: ###

* Todo list.
* Finish off the component diagram.
